package com.example.appmenubutton.database

import java.io.Serializable

data class AlumnoLista(
    var id: Int,
    var matricula: String,
    var nombre: String,
    var domiciio: String,
    var especilidad: String,
    var foto: String
) : Serializable



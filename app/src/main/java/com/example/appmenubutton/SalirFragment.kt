package com.example.appmenubutton

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubutton.database.Alumno
import com.example.appmenubutton.database.AlumnoLista
import com.example.appmenubutton.database.MiAdaptador
import com.example.appmenubutton.database.dbAlumnos
import com.google.android.material.floatingactionbutton.FloatingActionButton
//import dbFragment

class SalirFragment : Fragment(), MiAdaptador.OnItemClickListener, AlumnoListener {
    private lateinit var rc: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var listaAlumnos: ArrayList<AlumnoLista>
    private lateinit var fab: FloatingActionButton
    private lateinit var db: dbAlumnos
    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_salir, container, false)

        rc = view.findViewById(R.id.recId)
        fab = view.findViewById(R.id.agregarAlumno)
        searchView = view.findViewById(R.id.searchView)
        listaAlumnos = ArrayList()
        adaptador = MiAdaptador(listaAlumnos, requireContext(), this)

        rc.layoutManager = LinearLayoutManager(context)
        rc.adapter = adaptador

        db = dbAlumnos(requireContext())
        db.openDataBase()
        cargaAlumnos()
        db.close()

        fab.setOnClickListener {
            cambiarDbFragment()
        }

        setupSearchView()

        return view
    }

    private fun setupSearchView() {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // Remover espacios antes de filtrar
                val filteredText = newText?.replace("\\s+".toRegex(), "") ?: ""
                adaptador.filter.filter(filteredText)
                return true
            }
        })
    }

    private fun cargaAlumnos() {
        try {
            val alumnos = db.getAllAlumnos()
            listaAlumnos.clear()
            for (alumno in alumnos) {
                val alumnoLista = AlumnoLista(
                    id = alumno.id,
                    matricula = alumno.matricula,
                    nombre = alumno.nombre,
                    domiciio = alumno.domicilio,
                    especilidad = alumno.especialidad,
                    foto = alumno.foto
                )
                listaAlumnos.add(alumnoLista)
            }
            adaptador.notifyDataSetChanged()
        } catch (e: Exception) {
            Toast.makeText(requireContext(), "Error al cargar los alumnos", Toast.LENGTH_SHORT).show()
        }
    }

    private fun cambiarDbFragment() {
        val cambioFragment = dbFragment()
        cambioFragment.setTargetFragment(this, 1)
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.frmContenedor, cambioFragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    override fun onItemClick(alumno: AlumnoLista) {
        val fragment = dbFragment()
        val bundle = Bundle()
        bundle.putSerializable("mialumno", alumno)
        fragment.arguments = bundle

        fragmentManager?.beginTransaction()
            ?.replace(R.id.frmContenedor, fragment)
            ?.addToBackStack(null)
            ?.commit()
    }

    override fun onAlumnoAdded(alumno: Alumno) {
        val alumnoLista = AlumnoLista(
            id = alumno.id,
            matricula = alumno.matricula,
            nombre = alumno.nombre,
            domiciio = alumno.domicilio,
            especilidad =  alumno.especialidad,
            foto = alumno.foto
        )
        listaAlumnos.add(alumnoLista)
        adaptador.notifyItemInserted(listaAlumnos.size - 1)
        rc.scrollToPosition(listaAlumnos.size - 1) // Desplazarse a la posición del nuevo alumno
    }
}